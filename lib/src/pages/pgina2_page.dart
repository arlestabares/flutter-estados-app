import 'package:flutter/material.dart';
import 'package:flutter_app_estados/src/models/usuario.dart';
import 'package:flutter_app_estados/src/pages/services/usuario_service.dart';

class Pagina2page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: StreamBuilder(
          stream: usuarioService.usuarioStream,
          builder: (BuildContext context, AsyncSnapshot<Usuario> snapshot) {
            return snapshot.hasData
                ? Text('Nombre : ${snapshot.data.nombre}')
                : Text('Pagina 2');
          },
        ),
        //Text('Pagina 2'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            MaterialButton(
                child: Text(
                  'Establecer Usuario',
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                onPressed: () {
                  final nuevoUsuario = new Usuario(nombre: 'kamar', edad: 1);
                  usuarioService.cargarUsuario(nuevoUsuario);
                  //Navigator.pushNamed(context, 'pagina1');
                }),
            MaterialButton(
                child: Text(
                  'Cambiar Edad',
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                onPressed: () {
                  usuarioService.cambiarEdad(30);
                  // Navigator.pushNamed(context, 'pagina1');
                }),
            MaterialButton(
                child: Text(
                  'Añadir Profesion',
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                onPressed: () => {})
          ],
        ),
      ),
    );
  }
}
