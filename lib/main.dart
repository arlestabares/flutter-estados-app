import 'package:flutter/material.dart';
import 'package:flutter_app_estados/src/pages/pagina1_page.dart';
import 'package:flutter_app_estados/src/pages/pgina2_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
     initialRoute: 'pagina1',
     routes: {
      'pagina1': (_)=> Pagina1Page(),
      'pagina2': (_)=> Pagina2page(),
     },
    );
  }
}